Mason Manual Page
-----------------

Name
.....

mason - Mason image and container command line interface


Synopsis
........

**mason** --images-dir=<directory> [options]  <command> [options] <image>

**mason** [--help|--version]

Description
...........

Options
.......

--help
  show quick help

--images-dir=<directory>
  set the location of the images directory.

Commands
........

Use "mason help" or "mason --help" to get an overview of available commands.

Examples
........

mason help

mason view overlay org.example/category/example-brick

History
.......

July, 2018: Initial feature set started.
