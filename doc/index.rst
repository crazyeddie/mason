.. mason documentation master file, created by
   sphinx-quickstart on Thu Jul 19 02:10:14 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mason's documentation!
=================================

Mason is a Linux container system designed to:

#. System composition using multiple, fine-grained overlay roots.
#. A more complete set of networking tools to allow full control over linux network capabilities.


Contents:

.. toctree::
   :maxdepth: 2
   :glob:

   design/index
   user-docs/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

API documentation
=================

.. toctree::
   :maxdepth: 2

   api/library_root
