Image Format
================

Requirements
------------

At this point just need:

#. Image tree location
#. Image tree standard
#. Image description format

Design
-------

Image tree location
....................

Location is determined by one of the following, in order of priority:

#. Command line option; or
#. Environment variable; or
#. /var/mason/

Image tree standard
...................

.. code-block:: text
  :caption: Image directory structure

  + org/path/
  |  - description.yaml
  |  + overlay # filesystem addition/subtractions based on / reference
  |    + lib
  |    + usr/lib
  |    + etc



Image description
.................

Yaml file format because whatever.

.. code-block:: yaml
  :caption: Example image description file

  depends:
    - org/path/image1
    - org/path/image2

Images are added in order.  Dependencies of dependencies are added in order.

.. code-block:: yaml
  :caption: org.derp/tests/test0/description.yaml

  depends:

.. code-block:: yaml
  :caption: org.derp/tests/test1/description.yaml

  depends:

.. code-block:: yaml
  :caption: org.derp/tests/test2/description.yaml

  depends:
    - ./test0 # can be relative

.. code-block:: yaml
  :caption: org.derp/tests/test3/description.yaml

  depends:
    - /org.derp/tests/test1
    - /org.derp/tests/test0

.. code-block:: yaml
  :caption: org.derp/tests/test4/description.yaml

  depends:
    - ./test2
    - ./test3

With the above descriptions, the overlay ordering of test4 should be:

.. code-block:: text
  :caption: Resulting overlay lower.
  
  ${IMAGES}/org.derp/tests/test0/overlay:/${IMAGES}/org.derp/tests/test2/overlay:${IMAGES}/org.derp/tests/test1/overlay:${IMAGES}/org.derp/tests/test0/overlay:${IMAGES}/org.derp/tests/test4/overlay
Tests
------------

Overlay tree
...............

#. Start with a few different images with various dependencies.
#. Individually generate the overlay ordering from images
#. Verify the output.
