#ifndef MODULES_HPP
#define MODULES_HPP


namespace strange_crew { namespace application { namespace mason {

namespace detail_ {

struct option_builder {
    option_builder constexpr operator () (char,char const*,char const*) { return option_builder(); }

    void derp();

    constexpr option_builder() {}
};

}

constexpr detail_::option_builder options_builder() { return detail_::option_builder(); }

}}}


#endif