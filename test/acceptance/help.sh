#!/usr/bin/env bat


PATH=$PATH:../../src

@test "help content" {

   VERSION="0.0.1"

expected=$(cat help-pages/main-help)
#   source help-pages/main-help

   derp=${expected}
   result="$(mason --help)"

   [ $? -eq 0 ] 
   [ "$result" == "$expected" ]
}

#@test "no arguments" {
#    expected=$(cat help-pages/main-help)
#
#    result="$(mason)"
#
#    [ $? -ne 0 ]
#    [ "$result" == "$expected" ]
#}